<?php

namespace App\Http\Livewire\Order;

use App\Models\Order;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    public $id_order;
    public $name;
    public $quantity;
    public $total;
    public $payment;
    public $method;
    public $change;
    public $status;

    public function saveData()
    {
        if ($this->id_order) {
            $order = Order::where('id', $this->id_order)->first();
            $order->update([
                'id'       => $this->id_order,
                'name'     => $this->name,
                'quantity' => $this->quantity,
                'total'    => $this->total,
                'payment'  => $this->payment,
                'method'   => $this->method,
                'change'   => $this->change,
                'status'   => $this->status,
            ]);
        }else{
            Order::create([
                'id'       => $this->id_order,
                'name'     => $this->name,
                'quantity' => $this->quantity,
                'total'    => $this->total,
                'payment'  => $this->payment,
                'method'   => $this->method,
                'change'   => $this->change,
                'status'   => $this->status,
            ]);
        }

        $this->reset();
    }

    public function deleteData($id)
    {
        Order::where('id', $id)->delete();
    }

    public function editData($id)
    {
        $order          = Order::where('id', $id)->first();
        $this->id_order = $order->id;
        $this->name     = $order->name;
        $this->quantity = $this->quantity;
        $this->total    = $this->total;
        $this->payment  = $this->payment;
        $this->method   = $this->method;
        $this->change   = $this->change;
        $this->status   = $this->status;
    }

    public function render()
    {
        $data['orders'] = Order::paginate(5);

        return view('livewire.order.index', $data)->with('i');
    }
}
