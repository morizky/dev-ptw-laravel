<?php

namespace App\Http\Livewire\Product;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    public $id_product;
    public $id_category;
    public $name;
    public $stock;
    public $price;
    public $sold;
    public $status;

    public function saveData()
    {
        if ($this->id_product) {
            $product = Product::where('id', $this->id_product)->first();
            $product->update([
                'id_category' => $this->id_category,
                'name'        => $this->name,
                'stock'       => $this->stock,
                'price'       => $this->price,
                'sold'        => $this->sold,
                'status'      => $this->status,
            ]);
        }else{
            Product::create([
                'id_category' => $this->id_category,
                'name'        => $this->name,
                'stock'       => $this->stock,
                'price'       => $this->price,
                'sold'        => $this->sold,
                'status'      => $this->status,
            ]);
        }

        $this->reset();
    }

    public function deleteData($id)
    {
        Product::where('id', $id)->delete();
    }

    public function editData($id)
    {
        $product           = Product::where('id', $id)->first();
        $this->id_product  = $product->id;
        $this->id_category = $product->id_category;
        $this->name        = $product->name;
        $this->stock       = $product->stock;
        $this->price       = $product->price;
        $this->sold        = $product->sold;
        $this->status      = $product->status;
    }

    public function render()
    {
        $data['products'] = Product::paginate(5);

        return view('livewire.product.index', $data)->with('i');
    }
}
