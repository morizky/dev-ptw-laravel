<?php

namespace App\Http\Livewire\Category;

use App\Models\Category;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    public $id_category;
    public $name;

    public function saveData()
    {
        if ($this->id_category) {
            $category = Category::where('id', $this->id_category)->first();
            $category->update([
                'id'   => $this->id_category,
                'name' => $this->name,
            ]);
        }else{
            Category::create([
                'id'   => $this->id_category,
                'name' => $this->name,
            ]);
        }

        $this->reset();
    }

    public function deleteData($id)
    {
        Category::where('id', $id)->delete();
    }

    public function editData($id)
    {
        $category          = Category::where('id', $id)->first();
        $this->id_category = $category->id;
        $this->name        = $category->name;
    }

    public function render()
    {
        $data['categories'] = Category::paginate(5);

        return view('livewire.category.index', $data)->with('i');
    }
}
