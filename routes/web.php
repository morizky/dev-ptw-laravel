<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('artikel', function () {
    return view('artikel-0294');
});

Route::get('contact', function () {
    return view('contact-0294');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/product', function () {
    return view('product.index');
})->name('product');

Route::middleware(['auth:sanctum', 'verified'])->get('/category', function () {
    return view('category.index');
})->name('category');

Route::middleware(['auth:sanctum', 'verified'])->get('/order', function () {
    return view('order.index');
})->name('order');
