<?php
use App\Models\Product;

$products = Product::orderBy('created_at', 'DESC')->get(); ?>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="grid sm:grid-cols-2 md:grid-cols-4 gap-7 pb-10">
                <div class="bg-white p-4 rounded-lg shadow-lg">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2">
                            <p class="font-bold text-sm text-gray-400">Penjualan</p>
                            <p class="text-lg font-bold">12 Item</p>
                            <p class="text-gray-500">Dalam 1 minggu ini</p>
                        </div>
                        <div class="justify-self-end">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 bg-blue-500 text-white rounded-full p-3 shadow-md" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 8v8m-4-5v5m-4-2v2m-2 4h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                            </svg>
                        </div>
                    </div>
                </div>

                <div class="bg-white p-4 rounded-lg shadow-lg">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2">
                            <p class="font-bold text-sm text-gray-400">Produk</p>
                            <p class="text-lg font-bold">{{ DB::table('products')->count() }} Item</p>
                            <p class="text-gray-500">Total keseluruhan</p>
                        </div>
                        <div class="justify-self-end">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 bg-blue-500 text-white rounded-full p-3 shadow-md" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4" />
                            </svg>
                        </div>
                    </div>
                </div>

                <div class="bg-white p-4 rounded-lg shadow-lg">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2">
                            <p class="font-bold text-sm text-gray-400">Stok</p>
                            <p class="text-lg font-bold">{{ DB::table('products')->sum('stock') }} Unit</p>
                            <p class="text-gray-500">Untuk hari ini</p>
                        </div>
                        <div class="justify-self-end">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 bg-blue-500 text-white rounded-full p-3 shadow-md" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                            </svg>
                        </div>
                    </div>
                </div>

                <div class="bg-white p-4 rounded-lg shadow-lg">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2">
                            <p class="font-bold text-sm text-gray-400">Pegawai</p>
                            <p class="text-lg font-bold">{{ DB::table('users')->count() }} Orang</p>
                            <p class="text-gray-500">Total aktif</p>
                        </div>
                        <div class="justify-self-end">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 bg-blue-500 text-white rounded-full p-3 shadow-md" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>


            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <div class="px-6 py-4 border-0">
                        <h3 class="font-bold text-lg text-blueGray-700">Daftar Produk</h3>
                    </div>
                    <div class="block w-full">
                        <table class="items-center w-full">
                            <thead class="border-b-2">
                                <tr>
                                    <th class="px-6 py-3 text-xs uppercase font-bold text-left bord">ID</th>
                                    <th class="px-6 py-3 text-xs uppercase font-bold text-left bord">Nama</th>
                                    <th class="px-6 py-3 text-xs uppercase font-bold text-left bord">Harga</th>
                                    <th class="px-6 py-3 text-xs uppercase font-bold text-left bord">Stok</th>
                                    <th class="px-6 py-3 text-xs uppercase font-bold text-left bord">Terjual</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $item)
                                <tr>
                                    <td class="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                        <span class="font-bold">{{ $item->id }}</span>
                                    </td>
                                    <td class="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                        {{ $item->name }}
                                    </td>
                                    <td class="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                        Rp. {{ number_format($item->price) }}
                                    </td>
                                    <td class="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                        {{ $item->stock }} Pcs
                                    </td>
                                    <td class="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                        {{ $item->sold }} Pcs
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="px-4 py-5 flex-auto ct-docs-frame">
                            <div class="relative flex flex-wrap justify-center ">
                                <div class="w-full">
                                    <nav class="block">
                                        <ul class="flex pl-0 rounded list-none flex-wrap">
                                            <!-- <li>
                                                <a href="#" class="w-8 h-8 text-xs font-semibold flex mx-1 p-0 rounded-full items-center justify-center border border-solid transition-all duration-150 ease-in-out bg-blue-500 border-blue-500 text-white hover:border-blue-700 hover:text-white hover:bg-blue-700">1</a>
                                            </li>
                                            <li>
                                                <a href="#" class="w-8 h-8 text-xs font-semibold flex mx-1 p-0 rounded-full items-center justify-center border border-solid transition-all duration-150 ease-in-out bg-transparent border-blue-500 text-blue-500 hover:border-blue-700 hover:text-blue-700">2</a>
                                            </li> -->
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</x-app-layout>
