
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Pembelian') }}
    </h2>
</x-slot>

<div x-data="{ showModal : false }">
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="px-6 py-4 border-0">
                    <h3 class="font-bold text-lg text-blueGray-700">Daftar Pembelian</h3>

                    <x-jet-button @click="showModal = !showModal" class="mt-5">
                        {{ __('Tambah') }}
                    </x-jet-button>

                </div>
                <div class="block w-full">
                    <table class="items-center w-full">
                        <thead class="border-b-2">
                            <tr>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">Nomor</th>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">ID</th>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">Nama</th>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">Produk</th>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">Total</th>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">Metode</th>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">Pembayaran</th>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">Kembalian</th>
                                <th class="px-6 py-3 text-sm uppercase font-bold text-left bord">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $item)
                            <tr>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    {{ ++$i }}
                                </td>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    #{{ $item->id }}
                                </td>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    {{ $item->name }}
                                </td>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    {{ $item->product->name }}
                                </td>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    Rp. {{ number_format($item->total) }}
                                </td>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    {{ $item->method }}
                                </td>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    Rp. {{ number_format($item->payment) }}
                                </td>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    Rp. {{ number_format($item->change) }}
                                </td>
                                <td class="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                                    <x-jet-button wire:click="editData('{{$item->id}}')" @click="showModal = !showModal">
                                        {{ __('Ubah') }}
                                    </x-jet-button>
                                    <x-jet-button wire:click="deleteData('{{$item->id}}')" wire:loading.attr="disabled">
                                        {{ __('Hapus') }}
                                    </x-jet-button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="px-4 py-5 flex-auto ct-docs-frame">
                        <div class="relative flex flex-wrap justify-center ">
                            <div class="w-full">
                                <nav class="block">
                                    <ul class="flex pl-0 rounded list-none flex-wrap">
                                        {{$orders->links()}}
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div x-show="showModal" @click.away="showModal = false" class="fixed z-10 inset-0 overflow-y-auto" style="display:none" aria-labelledby="modal-title" role="dialog" aria-modal="true" x-transition:enter="transition ease duration-200 transform" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition ease duration-300" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
            <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">

                <form wire:submit.prevent="saveData">
                    <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                            {{ __('Tambah Pembelian') }}
                        </h3>
                        <div class="mt-2">

                            <div class="mt-4">
                                <x-jet-label for="name" value="{{ __('Nama') }}" class="mt-1"/>
                                <x-jet-input type="text" class="mb-5 block w-full" placeholder="{{ __('Nama') }}" wire:model="name"/>
                                <x-jet-label for="id_product" value="{{ __('Produk') }}" class="mt-1"/>
                                <x-jet-input type="text" class="mb-5 block w-full" placeholder="{{ __('Produk') }}" wire:model="id_product"/>
                                <div class="flex mb-5">
                                    <div class="w-6/12 pr-1">
                                        <x-jet-label for="quantity" value="{{ __('Kuantitas') }}" class="mt-1"/>
                                        <x-jet-input type="number" class="block w-full" placeholder="{{ __('Kuantitas') }}" wire:model="quantity"/>
                                    </div>
                                    <div class="w-6/12 pl-1">
                                        <x-jet-label for="total" value="{{ __('Total') }}" class="mt-1"/>
                                        <x-jet-input type="number" class="block w-full" placeholder="{{ __('Total') }}" wire:model="total"/>
                                    </div>
                                </div>
                                <div class="flex mb-2">
                                    <div class="w-6/12 pr-1">
                                        <x-jet-label for="method" value="{{ __('Metode') }}" class="mt-1"/>
                                        <x-jet-input type="number" class="block w-full" placeholder="{{ __('Metode') }}" wire:model="method"/>
                                    </div>
                                    <div class="w-6/12 pl-1">
                                        <x-jet-label for="payment" value="{{ __('Pembayaran') }}" class="mt-1"/>
                                        <x-jet-input type="text" class="mb-2 block w-full" placeholder="{{ __('Pembayaran') }}" wire:model="payment"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-gray-50 px-4 py-4 sm:px-6 sm:flex sm:flex-row-reverse">
                        <x-jet-button class="inline-flex ml-2" wire:loading.attr="disabled" type="submit">
                            {{ __('Simpan') }}
                        </x-jet-button>
                        <x-jet-secondary-button @click="showModal = !showModal">
                            {{ __('Batal') }}
                        </x-jet-secondary-button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
